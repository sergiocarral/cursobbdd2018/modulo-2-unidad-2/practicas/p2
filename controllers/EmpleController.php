<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    // Mostrar todos los campos y todos los 
    // registros de la tabla empleado
    public function actionConsulta1(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find();
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta1",[
           "dataProvider"=>$d,
       ]);
    }
       
     public function actionConsulta3(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find();
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta3",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta6(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()->count();
       
              
       return $this->render("consulta6",[
           "d"=>$r,
       ]);
        
    }
    
     public function actionConsulta7(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()->orderBy("apellido");
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta7",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta8(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()->orderBy(["apellido"=>SORT_DESC]);
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta8",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta10(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()->orderBy([
           "dept_no"=>SORT_DESC,
           ]);
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta10",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta11(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()->orderBy([
           "dept_no"=>SORT_DESC,
           "oficio"=>SORT_ASC,
           ]);
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta11",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta12(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()->orderBy([
           "dept_no"=>SORT_DESC,
           "apellido"=>SORT_ASC,
           ]);
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta12",[
           "dataProvider"=>$d,
       ]);
        
    }
    
    public function actionConsulta13(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->select('emp_no')
               ->where(['>','salario','2000']);
           
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
             
       
              
       return $this->render("consulta13",[
           "dataProvider"=>$d,
       ]);
        
    }
    
    public function actionConsulta14(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->select('emp_no,apellido')
               ->where(['<','salario','2000']);
           
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta14",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta15(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where(['between','salario',1500,2500]);
           
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta15",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta16(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where("oficio like 'analista'");
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta16",[
           "dataProvider"=>$d,
       ]);
        
    }
    
    public function actionConsulta17(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where("oficio like 'analista'
                        and
                       'salario' < 2000"
                       );
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta17",[
           "dataProvider"=>$d,
       ]);
        
    }
    
    public function actionConsulta18(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where('dept_no=20');
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta18",[
           "dataProvider"=>$d,
       ]);
        
    }
    
    public function actionConsulta19(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where("oficio like 'vendedor'")
               ->count();
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       /*$d=new ActiveDataProvider([
           "query"=>$r,
       ]);*/
       
              
              
       return $this->render("consulta19",[
           "d"=>$r,
       ]);
        
    }
    
    public function actionConsulta20(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where("apellido like 'm%' or 'n%'")
               ->orderBy(['apellido'=>SORT_ASC]);
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta20",[
           "dataProvider"=>$d,
       ]);
        
    }
    
     public function actionConsulta21(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
       $r=Emple::find()
               ->where("oficio like 'vendedor'")
               ->orderBy([
                   'apellido'=>SORT_ASC
               ]);
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta21",[
           "dataProvider"=>$d,
       ]);
        
    }
    
    public function actionConsulta22(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
        
       $max=Emple::find()
               ->max('salario');
        
       $r= Emple::find()
               ->select('apellido')
               ->where(['salario' => $max]);
               
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta22",[
           "dataProvider"=>$d,
       ]);
    }
       
       public function actionConsulta23(){
       /**
        * voy a utilizar ActiveQuery
        * desde ActiveRecord
        */ 
        
       $r= Emple::find()
               ->where(['and',('dept_no=10'),("oficio = 'analista'")])
               ->orderBy(['apellido' => SORT_ASC,'oficio' => SORT_ASC]);
       
              
       
       /**
        * voy a utilizar un dataprovider 
        * desde activeQuery
        */
       
       $d=new ActiveDataProvider([
           "query"=>$r,
       ]);
       
              
              
       return $this->render("consulta23",[
           "dataProvider"=>$d,
       ]);
        
    }
}
